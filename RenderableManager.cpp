//
//  RenderableManager.cpp
//  PolterheistTest
//
//  Created by Evan Chapman on 2/24/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "RenderableManager.h"

/*
 ====================
 RenderableManager::RenderableManager
 ====================
 */
///<Constructor
RenderableManager::RenderableManager(){
    cout << "Created renderable manager" << endl;
}


/*
 ====================
 RenderableManager::~RenderableManager
 ====================
 */
///<Destructor
RenderableManager::~RenderableManager(){
    cout << "Destroyed Renderable Manager" << endl;
}

/*
 ====================
 RenderableManager::Init
 ====================
 */
void RenderableManager::Init(){
    cout << "Initializing renderable Manager" << endl;
}


/*
 ====================
 RenderableManager::AddRenderable
 ====================
 */
///<Adds a renderable to the manager
void RenderableManager::AddRenderable(RenderableHandle newRenderable){
    m_Renderables.push_back(newRenderable);
}


/*
 ====================
 RenderableManager::RemoveRenderable
 ====================
 */
///<Removes a renderable from the manager
void RenderableManager::RemoveRenderable(RenderableHandle deadRenderable){
    RenderableIterator current = m_Renderables.begin();
    while (current != m_Renderables.end()){
        if (*current == deadRenderable){
            current = m_Renderables.erase(current);
        } else {
            current++;
        }
    }
}


/*
 ====================
 RenderableManager::NextRenderable
 ====================
 */
///<Returns the next Rendable in line
RenderableHandle RenderableManager::NextRenderable(){
    //Make sure we are not at end
    if (!IsAtEnd()){
        m_CurrentRenderable++;
    }
    return m_Renderables[m_CurrentRenderable];
}


/*
 ====================
 RenderableManager::PrevRenderable
 ====================
 */
///<Returns the previous renderable in the list
RenderableHandle RenderableManager::PrevRenderable(){
    //Make sure we are not at beginning
    if (!IsAtBegin()){
        m_CurrentRenderable--;
    }
    return m_Renderables[m_CurrentRenderable];
}


/*
 ====================
 RenderableManager::IsAtBegin
 ====================
 */
///<Returns true if the list is at the beginning
bool RenderableManager::IsAtBegin(){
    return m_CurrentRenderable <= 0 ;
}


/*
 ====================
 RenderableManager::IsAtEnd
 ====================
 */
///<Returns true if list is at end
bool RenderableManager::IsAtEnd(){
    return m_CurrentRenderable >= m_Renderables.size();;
}


/*
 ====================
 RenderableManager::IsEmpty
 ====================
 */
///<Returns true if list is empty
bool RenderableManager::IsEmpty(){
    return m_Renderables.size() <= 0;
}


/*
 ====================
 RenderableManager::Clear
 ====================
 */
///<Empties out the list
void RenderableManager::Clear(){
    if (!IsEmpty()){
        m_Renderables.clear();
    }
}


/*
 ====================
 RenderableManager::Begin
 ====================
 */
///<Returns to the beginning of the list of renderables
RenderableHandle RenderableManager::FirstRenderable(){
    m_CurrentRenderable = 0;
    return m_Renderables[m_CurrentRenderable];
}



































































//END