//
//  Random.cpp
//  PolterheistTest
//
//  Created by Evan Chapman on 3/3/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "Random.h"

void Random::Seed(){
    srand(static_cast<unsigned int>(time(0)));
}