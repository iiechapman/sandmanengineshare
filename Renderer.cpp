//
//  Renderer.cpp
//  PolterheistTest
//
//  Created by Evan Chapman on 2/22/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "Renderer.h"



Renderer::Renderer(){
    cout << "False Constructor" << endl;
}


Renderer::~Renderer(){
    cout << "Destroyed renderer" << endl;
}


///<Recommended Constructor
Renderer::Renderer(SDL_Renderer* newFrame){
    Init();
    if (!SetFrame(newFrame)){//Setframe failed
        cout << "Could not set renderer" << endl;
        SetFrame(0); //Detach frame and attempt to set later
        m_IsFrameConnected = false;
    } else {
        m_IsFrameConnected = true;
    }
}


void Renderer::Init(){
    cout << "Initializing renderer..." << endl;
}


///<Returns whether or not frame is built and ready for render
const bool Renderer::IsFrameBuilt()const{
    return m_IsFrameBuilt;
}


///<Copies all renderables to frame before being rendered to screen
void Renderer::BuildFrame(){
    auto render = m_Renderables->FirstRenderable();

    while (!m_Renderables->IsAtEnd()) {
        
        SDL_SetRenderDrawColor(m_Frame,
                               render->GetColor().r,
                               render->GetColor().g,
                               render->GetColor().b,
                               render->GetColor().a);
        
        
        SDL_RenderFillRect(m_Frame, &render->GetDestRect());
        
        //TODO: Add texture ability, use this code
//        SDL_RenderCopyEx(m_Frame, render->GetTexture(), &render->GetSourceRect(),
//                         &render->GetDestRect(), render->GetAngle(),
//                         &render->GetCenter(), render->GetFlip()
//                         );
        
        render = m_Renderables->NextRenderable();
    }
    
    m_IsFrameBuilt = true;
}


void Renderer::Render(){
    if (m_IsFrameConnected)
    {
        SDL_SetRenderDrawColor(m_Frame ,0,0,0,0);
        SDL_RenderClear(m_Frame);
        
        if (IsFrameBuilt()){
            SDL_RenderPresent(m_Frame);
        } else {
            BuildFrame();
            SDL_RenderPresent(m_Frame);
        }
    }
    m_IsFrameBuilt = false;//Frame is now old, needs to be redrawn
}


///<Connects renderer to SDL_Renderer surface
bool Renderer::SetFrame(SDL_Renderer* newFrame){
    if (newFrame){              //If frame exists then connect
        m_Frame = newFrame;
        m_IsFrameConnected = true;
    } else {                    //Otherwise, clear out and fail
        m_Frame = 0;
        m_IsFrameConnected = false;
    }
    
    return m_IsFrameConnected;
}


void Renderer::AttachManager(RenderableManager* manager){
    m_Renderables = manager;
}
































//END