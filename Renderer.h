//
//  Renderer.h
//  PolterheistTest
//
//  Created by Evan Chapman on 2/22/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __PolterheistTest__Renderer__
#define __PolterheistTest__Renderer__

#include <iostream>
#include <SDL2/SDL.h>
#include "RenderableManager.h"

using namespace std;

class Renderer{
public:
    Renderer(SDL_Renderer* newFrame);
    ~Renderer();
    
    void            Init();
    void            Render();
    void            AttachManager(RenderableManagerHandle renderables);
    bool            SetFrame(SDL_Renderer* newFrame);
    
protected:
    void            BuildFrame();
    const bool      IsFrameBuilt()const;

    
private:
    Renderer();//Hide empty constructor
    RenderableManagerHandle m_Renderables;
    SDL_Renderer* m_Frame;
    bool m_IsFrameBuilt;
    bool m_IsFrameConnected;
    
};


#endif /* defined(__PolterheistTest__Renderer__) */
























































//END