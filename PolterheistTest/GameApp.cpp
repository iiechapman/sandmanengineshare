//
//  GameApp.cpp
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/30/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.


#include <vector>
#include "GameApp.h"
#include <SDL2_image/SDL_image.h>
#include <SDL2_net/SDL_net.h>
#include <iostream>
#include <string>

using namespace std;

/*
 ===============================================================================
 
 GameApp.cpp
 Contains main game loop and connects classes together
 
 ===============================================================================
 */


#ifdef APPLE
#pragma mark GameApp
#endif

/*
 ====================
 GameApp::GameApp
 ====================
 */
GameApp::GameApp()
:FALSE_TIMER(0){
    if ( DEBUG_MODE_ON ){
        cout << "Begin Game " << endl;
    }
}


/*
 ====================
 GameApp::~GameApp
 ====================
 */
GameApp::~GameApp(){
    if ( DEBUG_MODE_ON ){
        cout << "Ending game " << endl;

        delete renderer;
        delete renderableManager;
        renderer = 0;
        renderableManager = 0;
    }
}

/*
 =====================
 GameApp::Destroy
 =====================
 */

void GameApp::Destroy( void )
{
    if ( DEBUG_MODE_ON ){
        cout << "Destroying app... " << endl;
    }
    App::destroy();
}


#ifdef APPLE
#pragma mark  -
#pragma mark Init
#endif

/*
 =====================
 GameApp::Init
 =====================
 */
const int GameApp::Init( void )
{
    SeedRNG();
    
    renderer = new Renderer(sdl_renderer);

    InitDebug();
    return APP_OK;
}


#ifdef APPLE
#pragma mark  -
#pragma mark Game Loop
#endif

/*
 =====================
 GameApp::Run
 =====================
 */
const int GameApp::Run( int width, int height, string title ){
    App::run( width,height ,title );
    running = true;

    Init();

    //Game Loop
    while ( running )
    {
   
        //Event Handling
        SDL_Event ev;
        while ( SDL_PollEvent( &ev ) ){
            HandleEvents( &ev );
        }
        
        Update(FALSE_TIMER);
        Render();
        
        //Quit Loops
        if (!running){
            break;
        }
    }
    
    return APP_OK;
}


/*
 ====================
 GameApp::Update
 ====================
 */
///<Updates the simulation
void GameApp::Update(uint delta){
    //Run tests
    Debug();

}

void GameApp::Render(){
    renderer->Render();
}



#ifdef APPLE
#pragma mark -
#pragma mark Debug Section
#endif

/*
 ====================
 GameApp::InitDebug
 ====================
 */
///<Intitializes the debugging test environment
void GameApp::InitDebug(){
    
    //Testing Renderable and renderable manager
    renderableManager = new RenderableManager();
    
    //Loop through and make X amount of renderables
    for (int i = 0 ; i < 30 ; i++)
    {
    Renderable* render = new Renderable();
    
    render->SetAngle(0);
    
    SDL_Point center;
    center.x = 0;
    center.y = 0;
    
    render->SetCenter(center);
    
    SDL_Rect destRect;
    destRect.x = GetRandomNumber(10, 800);
    destRect.y = GetRandomNumber(10, 800);
    destRect.w = GetRandomNumber(100, 800);
    destRect.h = GetRandomNumber(100, 800);
    render->SetDestRect(destRect);
    
    SDL_Rect sourceRect;
    sourceRect.x = 0;
    sourceRect.y = 0;
    sourceRect.w = 0;
    sourceRect.h = 0;
    
    render->GetColor().r = GetRandomNumber(254, 255);
    render->GetColor().g = GetRandomNumber(10, 50);
    render->GetColor().b = GetRandomNumber(10, 50);
    
    render->SetSourceRect(sourceRect);
    
    render->SetVisibility(true);
    render->SetTexture(NULL);
    render->SetFlip(SDL_FLIP_NONE);


    //Add test renderable to manager
    renderableManager->AddRenderable(render);
        render = 0;
    }
    
    
    renderer->AttachManager(renderableManager);
    
}


/*
 ====================
 GameApp::Debug
 ====================
 */
///<Test Stub Function
void GameApp::Debug(){

}


void GameApp::SeedRNG(){
    srand ( int ( time(0)) );
}

int GameApp::GetRandomNumber(int low, int high){
    return low + ((rand() % high) - low);
}






























//END