//
//  VisualProperties.h
//  Sandman
//
//  Created by Evan Chapman on 1/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __Sandman__VisualProperties__
#define __Sandman__VisualProperties__

#include <iostream>
#include <SDL2/SDL.h>

/*
 ===============================================================================
 
 cell_t
 A single cell indexed by a row and a column
 
 ===============================================================================
 */
struct cell_t{
                    cell_t();
    
    int             row;
    int             column;
    int             rowMax;
    int             columnMax;

    void            Clear( void );
    void            PrintOut( void );
};

/*
 ===============================================================================
 
 color_t
 Color adapter for SDL_color, extends abilities
 
 ===============================================================================
 */
struct color_t{
                    color_t();
    
    int             red,green,blue,alpha;
    
    void            Clear( void );
    SDL_Color*      ToColor( void );
    void            PrintOut( void );
};

#endif /* defined(__Sandman__VisualProperties__) */
