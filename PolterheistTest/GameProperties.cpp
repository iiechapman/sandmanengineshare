//
//  GameProperties.cpp
//  Sandman
//
//  Created by Evan Chapman on 1/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "GameProperties.h"
using namespace std;

#ifdef APPLE
#pragma mark  -
#pragma mark Timer
#endif

/*
 ====================
 Timer::Timer
 ====================
 */
Timer::Timer(){
    lastTime = 0;
    delta = 0;
}


/*
 ====================
 Timer::~Timer
 ====================
 */
Timer::~Timer(){
    
}


/*
 ====================
 Timer::Start
 ====================
 */
void Timer::Start( void ){
    timerRunning = true;
}


/*
 ====================
 Timer::Stop
 ====================
 */
void Timer::Stop(){
    timerRunning = false;
    totalTimeElapsed = 0;
}


/*
 ====================
 Timer::Reset
 ====================
 */
void Timer::Reset(){
    totalTimeElapsed = 0;
}


/*
 ====================
 Timer::SetBeginTime
 ====================
 */
const Uint32 Timer::SetBeginTime(){
    lastTime = SDL_GetTicks();
    return lastTime;
}


/*
 ====================
 Timer::CalculateDelta
 ====================
 */
const Uint32 Timer::SetEndTime(){
    delta = SDL_GetTicks() - lastTime;
    return delta;
}


/*
 ====================
 Timer::Delta
 ====================
 */
const Uint32 Timer::Delta() const{
    return delta;
}

/*
 ====================
 Timer::GetElapsedTime
 ====================
 */
const Uint32 Timer::GetElapsedTime( void ) const{
    return totalTimeElapsed;
}


/*
 ====================
 Timer::Update
 ====================
 */
void Timer::Update(){
    didTick = false;
    
    SetEndTime();
    SetBeginTime();
    
    if ( timerRunning ){
        totalTimeElapsed += delta;
    }
    
    if ( totalTimeElapsed >= tickTime && timerRunning ){
        didTick = true ;
        Stop();
    }
}


/*
 ====================
 Timer::Tick
 ====================
 */
const bool Timer::Tick( void ) const{
    return didTick;
}


/*
 ====================
 Timer::SetLooping
 ====================
 */
void Timer::SetLooping( const bool isLooping ){
    timerLooping = isLooping;
}


/*
 ====================
 Timer::SetTickTime
 ====================
 */
void Timer::SetTickTime( const Uint32 newTickTime ){
    tickTime = newTickTime;
}


/*
 ====================
 Timer::PrintOut
 ====================
 */
void Timer::PrintOut( void ) const{
    cout << "delta: " << delta << endl;
    cout << "lastTime: " << lastTime << endl;
    cout << "totalTimeElapsed: " << totalTimeElapsed << endl;
    cout << "tickTime: " << tickTime << endl;
    cout << "timerRunning: " << timerRunning << endl;
    cout << "didTick: " << didTick << endl;
}


#ifdef APPLE
#pragma mark  -
#pragma mark SpriteSheet
#endif

/*
 ====================
 SpriteSheet::SpriteSheet
 ====================
 */
SpriteSheet::SpriteSheet()
{
    
}



/*
 ====================
 SpriteSheet::~SpriteSheet
 ====================
 */

SpriteSheet::~SpriteSheet(){
    
}






























































