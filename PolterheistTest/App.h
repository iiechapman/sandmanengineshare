//
//  App.h
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/30/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#ifndef __GameEngineTests1__App__
#define __GameEngineTests1__App__

#include <iostream>
#include "PrefixHeader.pch"
#include "SDLApp.h"
using namespace std;
//Inherit the SDLApp for testing
class App : public SDLApp
{
public:
	// Initialize application, called by run(), don't call manually.
	int init(int width, int height, string title);
	
	// Destroy application, called by destructor, don't call manually.
	void destroy();
	
	// Run application, called by your code.
	int run(int width, int height , string title);
	
	// Called to process SDL event.
	void HandleEvents(SDL_Event* ev);
	
	// Called to render content into buffer.
	void Render();
};



#endif /* defined(__GameEngineTests1__App__) */
