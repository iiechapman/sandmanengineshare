//
//  Renderable.cpp
//  PolterheistTest
//
//  Created by Evan Chapman on 2/19/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "Renderable.h"
using namespace std;

#ifdef APPLE
#pragma mark Renderable
#endif


///<Constructor
Renderable::Renderable(){
    cout << "Creating renderable" << endl;
    Init();
}


///<Init
void Renderable::Init(){
    cout << "Initializing renderable..." << endl;
    
    m_Color.r = 255;
    m_Color.g = 255;
    m_Color.b = 255;
    m_Color.a = 255;
    
    m_DestRect.w = 10;
    m_DestRect.h = 10;
    m_DestRect.x = 10;
    m_DestRect.y = 10;
    
    m_IsVisible = true;
    m_Center = SDL_Point{0,0};
    
}


///<Destructor
Renderable::~Renderable(){
    cout << "Destroying renderable" << endl;
    m_Texture = 0;
}



///<Returns true if renderable is visible
bool Renderable::IsVisible(){
    return m_IsVisible;
}


///<Set the visibility of the renderable
void Renderable::SetVisibility(bool isVisible){
    m_IsVisible = isVisible;
}



SDL_Texture* Renderable::GetTexture(){
    return  m_Texture;
}

SDL_Color& Renderable::GetColor(){
    return m_Color;
}

SDL_Rect& Renderable::GetSourceRect(){
    return m_SourceRect;
}


SDL_Rect& Renderable::GetDestRect(){
    return m_DestRect;
}


double Renderable::GetAngle(){
    return m_Angle;
}


SDL_Point& Renderable::GetCenter(){
    return m_Center;
}


SDL_RendererFlip Renderable::GetFlip(){
    return m_Flip;
}


void Renderable::SetTexture(SDL_Texture* texture){
    m_Texture = texture;
}


void Renderable::SetColor(SDL_Color color){
    m_Color = color;
}

void Renderable::SetSourceRect(SDL_Rect sourceRect){
    m_SourceRect = sourceRect;
}

void Renderable::SetDestRect(SDL_Rect destRect){
    m_DestRect = destRect;
}

void Renderable::SetAngle(double angle){
    m_Angle = angle;
}

void Renderable::SetCenter(SDL_Point center){
    m_Center.x = center.x;
    m_Center.y = center.y;
}

void Renderable::SetFlip(SDL_RendererFlip flip){
    m_Flip = flip;
}



























//END