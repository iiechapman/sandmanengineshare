//
//  Renderable.h
//  PolterheistTest
//
//  Created by Evan Chapman on 2/19/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __PolterheistTest__Renderable__
#define __PolterheistTest__Renderable__

#include <iostream>
#include <SDL2/SDL.h>


/**
 * \class Renderable
 * \brief Contains all elements for the renderer to display texture to frame
 * \note Owned by Layer Object
 * \author Evan Chapman | vindince.com | iiechapman@gmail.com
 * \version 1
 * \date 02/22/2014
 * \note
 */

#include <vector>
using namespace std;

class Renderable;

using RenderableHandle = Renderable*;
using RenderableList = vector<RenderableHandle>;
using RenderableIndex = uint;
using RenderableIterator = RenderableList::iterator;


class Renderable{
public:
    Renderable();
    ~Renderable();
    
    void Init();
    
    SDL_Texture*            GetTexture();
    SDL_Color&              GetColor();
    SDL_Rect&               GetSourceRect();
    SDL_Rect&               GetDestRect();
    double                  GetAngle();
    SDL_Point&              GetCenter();
    SDL_RendererFlip        GetFlip();
    
    void                    SetTexture(SDL_Texture* texture);
    void                    SetColor(SDL_Color color);
    void                    SetSourceRect(SDL_Rect sourceRect);
    void                    SetDestRect(SDL_Rect destRect);
    void                    SetAngle(double angle);
    void                    SetCenter(SDL_Point center);
    void                    SetFlip(SDL_RendererFlip flip);

    bool                    IsVisible();
    void                    SetVisibility(bool isVisible);
    
private:
    bool                    m_IsVisible;    /**< Visibility Flag */
    SDL_Texture*            m_Texture;      /**< Current Texture */
    SDL_Color               m_Color;        /**< Current Color */
    SDL_Rect                m_SourceRect;   /**< Rect in text used for render */
    SDL_Rect                m_DestRect;     /**< Position of rendered texture on frame */
    double                  m_Angle;        /**< Angle of rotation */
    SDL_Point               m_Center;       /**< Center point for rotation */
    SDL_RendererFlip        m_Flip;         /**< Render Flip */
};


#endif /* defined(__PolterheistTest__Renderable__) */



































//END