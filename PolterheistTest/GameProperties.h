//
//  GameProperties.h
//  Sandman
//
//  Created by Evan Chapman on 1/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __Sandman__GameProperties__
#define __Sandman__GameProperties__

#include <iostream>
#include <SDL2/SDL.h>
#include "VisualProperties.h"

/*
 ===============================================================================
 
 Timer
 Timer that calculates Delta between iterations
 
 ===============================================================================
 */
class Timer{
public:
    
                    Timer();
                    ~Timer();
    
    const Uint32    SetBeginTime( void );
    const Uint32    SetEndTime( void );
    
    void            Update( void );
    const Uint32    Delta( void ) const;
    
    const Uint32    GetElapsedTime( void ) const;
    
    const bool      Tick( void ) const;
    void            SetTickTime( const Uint32 newTickTime );
    
    void            SetLooping( const bool isLooping );
    
    void            Start( void );
    void            Stop( void );
    void            Reset( void );
    
    void            PrintOut( void ) const;
    
private:
    Uint32          delta;
    Uint32          lastTime;
    Uint32          totalTimeElapsed;
    Uint32          tickTime;
    
    bool            timerRunning;
    bool            timerLooping;
    bool            didTick;
};


class SpriteSheet{
public:
                    SpriteSheet();
                    ~SpriteSheet();
    
    
private:
    cell_t          currentCell;
    
    
};

#endif /* defined(__Sandman__GameProperties__) */

























