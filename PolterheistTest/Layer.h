//
//  Layer.h
//  PolterheistTest
//
//  Created by Evan Chapman on 2/22/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __PolterheistTest__Layer__
#define __PolterheistTest__Layer__

#include <iostream>
#include <list>
#include <SDL2/SDL.h>
#include "Renderable.h"
using namespace std;


class Layer;
using LayerHandle = Layer*;
using LayerList = vector<LayerHandle>;


/**
 * \class Layer
 * \brief Contains and ordered list of renderables for the renderer
 * \note Contained in Scenes
 * \author Evan Chapman
 * \version 1
 * \date 02/22/2014
 * Contact: iiechapman\@gmail.com
 */


class Layer{
public:
    
    
private:
    RenderableList m_Renderables;
};


#endif /* defined(__PolterheistTest__Layer__) */







































































//END