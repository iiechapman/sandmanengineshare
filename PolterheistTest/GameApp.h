//
//  GameApp.h
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/30/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#ifndef __GameEngineTests1__GameApp__
#define __GameEngineTests1__GameApp__


#include <iostream>
#include <vector>
#include <random>
#include "App.h"
#include "GameEngine.h"
#include <vector>
#include "PrefixHeader.pch"


using namespace std;


/*
 ===================================
 GameApp
 Entire Game loop enclosed in code
 ===================================
 */

/**
 * \class GameApp
 * \brief Entire game loop enclosed in code
 * \note Starts automatically from main
 * \author Evan Chapman
 * \version 1
 * \date 02/22/2014
 * Contact: iiechapman\@gmail.com
 */

class GameApp : public App
{
public:
    GameApp();
    ~GameApp();
    
    const int           Init();
    const int           Run(int width, int height, string title);
    void                Update(uint delta);
    void                Destroy();
    
    //Render section
    vector<int>         intVector;
    Renderer*           renderer;
    RenderableManager*  renderableManager;
    
private:
    void                Render();
    void                Debug();
    void                InitDebug();
    
    //Replace with "random" object
    void                SeedRNG();
    int                 GetRandomNumber(int low, int high);
    
    
    //Replace with "timer" object
    const int           FALSE_TIMER;
    
};


#endif /* defined(__GameEngineTests1__GameApp__) */




















































































//END