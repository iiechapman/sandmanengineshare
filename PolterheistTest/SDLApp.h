//
//  SDLApp.h
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/28/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#ifndef __GameEngineTests1__SDLApp__
#define __GameEngineTests1__SDLApp__

#include <iostream>
#include <SDL2/SDL.h>
#include "PrefixHeader.pch"
using namespace std;


// SdlApplication is nothing more than thin wrapper to SDL library. You need
// just to instantiate it and call run() to enter the SDL event loop.
class SDLApp
{
public:
	SDLApp();
	~SDLApp();
	
	
	// Initialize application, called by run(), don't call manually.
	int init(int width, int height,string title);
	
	// Destroy application, called by destructor, don't call manually.
	void destroy();
	
	// Run application, called by your code.
	int run(int width, int height , string title);
	
	// Called to process SDL event.
	void HandleEvents(SDL_Event* ev);
	
	// Called to render content into buffer.
	void Render();
    
	
protected:
	// Whether the application is in event loop.
	bool running;
	SDL_Window *window;
	SDL_Renderer *sdl_renderer;
};




#endif /* defined(__GameEngineTests1__SDLApp__) */
