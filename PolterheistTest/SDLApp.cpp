//
//  SDLApp.cpp
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/28/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#include "SDLApp.h"

//Creates App
SDLApp::SDLApp() :running(false)
{
    
}

//Destroys app
SDLApp::~SDLApp()
{
	destroy();
}

//Inititalizes SDL window
int SDLApp::init(int width, int height , string title)
{
	// Initialize the SDL library.
	if (SDL_Init( SDL_INIT_VIDEO | SDL_INIT_TIMER ) < 0)
	{
		fprintf(stderr, "SDL_Init() failed: %s\n", SDL_GetError());
		return APP_FAILED;
	}
	
	window = SDL_CreateWindow(
                              title.c_str(),
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              width, height, SDL_WINDOW_SHOWN);
    
	sdl_renderer = SDL_CreateRenderer(
                                  window,
                                  -1,
                                  SDL_RENDERER_ACCELERATED
                                  );
    
	// Success.
	return APP_OK;
}

//Destroys window and renderer
void SDLApp::destroy()
{
	if ( window )
	{
		SDL_DestroyWindow(window);
		SDL_DestroyRenderer(sdl_renderer);
		SDL_Quit();
	}
}

//Start SDL App internal loop
int SDLApp::run( int width, int height , string title )
{
	// Initialize application.
	int state = init( width, height, title );
	if ( state != APP_OK ) return state;
	
	return APP_OK;
}

void SDLApp::HandleEvents(SDL_Event* ev)
{
	switch (ev->type)
	{
		case SDL_QUIT:
			running = false;
			break;
			
		case SDL_KEYDOWN:
		{
			if (ev->key.keysym.sym == SDLK_ESCAPE)
			{
				running = false;
			}
            
            if (ev->key.keysym.sym == SDLK_d)
			{
				cout << "AJ has hacked your machine" << endl;
                cout << "boohahaha" << endl;
			}
		}
        
	}
}

void SDLApp::Render()
{
    SDL_RenderPresent(sdl_renderer);
}




























