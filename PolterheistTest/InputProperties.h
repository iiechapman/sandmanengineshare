//
//  InputProperties.h
//  Sandman
//
//  Created by Evan Chapman on 1/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __Sandman__InputProperties__
#define __Sandman__InputProperties__

#include <iostream>
#include "PhysProperties.h"

using namespace std;

#ifdef APPLE
#pragma mark  -
#pragma mark Direction
#endif
enum direction_e{
    DIRECTION_UP,
    DIRECTION_DOWN,
    DIRECTION_RIGHT,
    DIRECTION_LEFT,
    DIRECTION_NO_VERTICAL,
    DIRECTION_NO_HORIZONTAL,
    DIRECTION_NONE
};

const int NUMBER_OF_DIRECTIONS = 7;

#ifdef APPLE
#pragma mark  -
#pragma mark Button State
#endif
enum button_state{
    BUTTON_PRESSED,
    BUTTON_HELD,
    BUTTON_RELEASED,
    BUTTON_ENABLED,
    BUTTON_DISABLED
};



/*
 ===============================================================================
 
 button_t
 Button type containing button state and name for referall
 
 ===============================================================================
 */
struct button_t{
    button_state    state = BUTTON_RELEASED;
    string          name;
    vec2_t          position;
    
    void            Press( void );
    void            Release( void );

    const bool      Active( void ) const;
    const bool      SinglePress( void ) const;
    const bool      Pressed( void ) const;
    const bool      Held( void ) const;
    const bool      Released(void ) const;

    
    void            Enable( void );
    void            Disable( void );
    
    void            Clear( void );
    void            PrintOut( void );
};



/*
 ===============================================================================
 
 direction_t
 A single direction, north south east or west
 
 ===============================================================================
 */
struct direction_t{
                    direction_t();
    
    direction_e     direction;
    
    void            PrintOut( void );
};



/*
 ===============================================================================
 
 directions_t
 A list of cardinal directions as booleans
 Returns true indicating something like a character moving up, or up and right
 
 ===============================================================================
 */
struct directions_t{
    directions_t(): up( false ), down( false ) , left( false ) , right( false ){};
    
    bool            up,down,left,right;
};


/*
 ===============================================================================
 
 controller_t
 Contains a group of buttons, may be changed later
 
 ===============================================================================
 */
struct controller_t{
    button_t up,down,left,right,button1,button2,fullScreenButton;
    
                    controller_t();
    void            PrintOut( void );
    const bool      NoDirectionPressed( void ) const;
    
};

#endif /* defined(__Sandman__InputProperties__) */


























