//
//  Random.h
//  PolterheistTest
//
//  Created by Evan Chapman on 3/3/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __PolterheistTest__Random__
#define __PolterheistTest__Random__

#include <iostream>
#include <random>
#include <ctime>

class Random{
public:
    Random();
    void Seed();
    void SetLow(int low);
    void SetHigh(int high);
    
private:
    int m_High;
    int m_Low;
    
};


#endif /* defined(__PolterheistTest__Random__) */
