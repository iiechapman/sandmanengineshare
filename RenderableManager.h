//
//  RenderableManager.h
//  PolterheistTest
//
//  Created by Evan Chapman on 2/24/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __PolterheistTest__RenderableManager__
#define __PolterheistTest__RenderableManager__

#include <iostream>
#include <vector>
#include "PrefixHeader.pch"
#include "Renderable.h"

using namespace std;

class RenderableManager;
using RenderableManagerHandle = RenderableManager*;


class RenderableManager{
    
public:
    RenderableManager();
    virtual ~RenderableManager();
    
    void                            Init();
    
    /*! \fn AddRenderable;
     *  \brief Adds renderable to render manager
     *  \param Takes a Renderable Handle
     *  \exception Should never take Null or 0,
     *  \returns true if theres enough space to handle new renderable
     */
    void                            AddRenderable(RenderableHandle newRenderable);
    void                            RemoveRenderable(RenderableHandle deadRenderable);
    RenderableHandle                FirstRenderable();
    RenderableHandle                NextRenderable();
    RenderableHandle                PrevRenderable();
    
    void                            Clear();
    bool                            IsAtEnd();
    bool                            IsAtBegin();
    bool                            IsEmpty();
    
private:
    RenderableList                  m_Renderables;
    RenderableIndex                 m_CurrentRenderable;

    
};


#endif /* defined(__PolterheistTest__RenderableManager__) */
